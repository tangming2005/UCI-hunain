---
title: "cd45-htseq-DEseq2"
output: html_document
---

I process the same data with STAR-htseq pipeline. parameters thanks to @Samir

```{r}
library(DESeq2)
library(tidyverse)
setwd("/home/mtang1/projects/Hunai_RNAseq/results/STAT-htseq")


samples<- c("Kdm2a_1291", "Kdm2a_1294", "Mll4_1612", "Mll4_1646", "WT_1665", "WT_1507")
condition<- c("Kdm2a", "Kdm2a", "Mll4", "Mll4", "WT", "WT")
meta<- data.frame(condition = condition)
rownames(meta)<- samples

directory <- "."

sampleFiles <- list.files(directory, pattern = ".cnt")
sampleCondition <- c("Kdm2a", "Kdm2a", "Mll4", "Mll4", "WT", "WT")
sampleTable <- data.frame(sampleName = samples,
                          fileName = sampleFiles,
                          condition = sampleCondition)

ddsHTSeq <- DESeqDataSetFromHTSeqCount(sampleTable = sampleTable,
                                       directory = directory,
                                       design= ~ condition)
ddsHTSeq


## pre-filter the low count genes
dds <- ddsHTSeq[ rowSums(counts(ddsHTSeq)) > 1, ]

dds$condition <- relevel(dds$condition, ref="WT")

dds <- DESeq(dds)

resultsNames(dds)

## specify contrast
res<- results(dds, contrast=c("condition","Mll4","WT"))

res$symbol<- rownames(res)
hist(res$pvalue)

as.data.frame(res) %>% filter(symbol == "Arid3a")
as.data.frame(res) %>% filter(symbol == "Casz1")
as.data.frame(res) %>% filter(symbol == "Epas1")
as.data.frame(res) %>% filter(symbol == "Casz1")
as.data.frame(res) %>% filter(symbol == "Mkl2")
as.data.frame(res) %>% filter(symbol == "Nfic")
as.data.frame(res) %>% filter(symbol == "Per2")
as.data.frame(res) %>% filter(symbol == "Smad6")
as.data.frame(res) %>% filter(symbol == "Sox6")
as.data.frame(res) %>% filter(symbol == "Tal1")
as.data.frame(res) %>% filter(symbol == "Tbx4")

as.data.frame(res) %>% filter(symbol == "Cdk1")
as.data.frame(res) %>% filter(symbol == "Tbx4")
as.data.frame(res) %>% filter(symbol == "Sox9")
as.data.frame(res) %>% filter(symbol == "Tp63")



write_tsv(as.data.frame(res), "~/projects/Hunai_RNAseq/results/STAT-htseq/DESeq2/mll4_vs_wt_htseq_DESeq2.tsv")
res_df<- as.data.frame(res)
res_df<- filter(res_df, !is.na(padj))

library(ggrepel)
plot_volcano<- function(toptable){
        results<- as.data.frame(toptable)
        results<- mutate(results, sig=ifelse(padj< 0.05, "FDR<0.05", "Not Sig"))

        p<-ggplot(results, aes(log2FoldChange, -log10(pvalue))) +
                geom_point(aes(col=sig)) +
                scale_color_manual(values=c("red", "black")) +
                ylim(c(0,50))

        p<- p+ geom_text_repel(data= dplyr::filter(results, padj < 0.01, abs(log2FoldChange) > 2.5), aes(label=symbol)) +
                theme_bw(base_size = 14)
        print (p)

}

plot_volcano(res_df)

```


PCA


```{r}

rld <- rlog(dds, blind=FALSE)
vsd <- varianceStabilizingTransformation(dds, blind=FALSE)
vsd.fast <- vst(dds, blind=FALSE)
head(assay(rld)[rownames(assay(rld)) != "", ])

plotPCA(rld, intgroup="condition")

## this will be used for GSEA analysis.
write.table(assay(rld)[rownames(assay(rld)) != "",3:6], "~/projects/Hunai_RNAseq/results/STAT-htseq/DESeq2/cd45_rld_htseq_DEseq2.tsv", col.names = T, row.names =T, quote = F, sep ='\t')
```

### compare salmon and HTseq results

```{r}

salmon_diff<- read_tsv("~/projects/Hunai_RNAseq/results/mll4_vs_wt.tsv", col_names = T)
htseq_diff<- read_tsv("~/projects/Hunai_RNAseq/results/STAT-htseq/DESeq2/mll4_vs_wt_htseq_DESeq2.tsv", col_names =T)


head(salmon_diff)

salmon_diff<- salmon_diff %>% dplyr::rename(logFC_salmon = log2FoldChange, padj_salmon = padj) %>%
  select(symbol, logFC_salmon, padj_salmon) %>% na.omit()

htseq_diff<-htseq_diff %>% dplyr::rename(logFC_htseq = log2FoldChange, padj_htseq = padj) %>%
  select(symbol, logFC_htseq, padj_htseq) %>% na.omit()

inner_join(salmon_diff,htseq_diff) %>% 
  ggplot() + geom_point(aes(x = logFC_salmon, y = logFC_htseq), alpha = 0.5, shape = 1) +
  coord_fixed() +
  geom_abline(slope = 1, intercept = 0, color = "red") +
  ggtitle("compare salmon and HTseq fold change")

```


ChIP-seq data

```{r}
library(DiffBind)
load("~/projects/Hunai_RNAseq/results/diffbind/UCI_H3K27ac_diffbind.rda")
load("~/projects/Hunai_RNAseq/results/diffbind/UCI_H3K4me1_diffbind.rda")
plot(UCI_H3K27ac)
plot(UCI_H3K4me1)

dba.plotPCA(UCI_H3K27ac,DBA_CONDITION,label=DBA_CONDITION)

dba.plotPCA(UCI_H3K4me1,DBA_CONDITION,label=DBA_CONDITION)

dba.plotPCA(UCI_H3K27ac, contrast=1,label=DBA_CONDITION)

dba.plotPCA(UCI_H3K4me1, contrast=1,label=DBA_CONDITION)

### annotate with ChIPseeker
library(ChIPseeker)
UCI_H3K4me1.DB
UCI_H3K27ac.DB


library(TxDb.Mmusculus.UCSC.mm9.knownGene)
library(org.Mm.eg.db)
txdb <- TxDb.Mmusculus.UCSC.mm9.knownGene
H3K27ac_diff_sites_anno <- annotatePeak(UCI_H3K27ac.DB, tssRegion=c(-3000, 3000), 
                         TxDb=txdb, annoDb="org.Mm.eg.db", level = "gene", overlap = "all")

H3K4me1_diff_sites_anno <- annotatePeak(UCI_H3K4me1.DB, tssRegion=c(-3000, 3000), 
                         TxDb=txdb, annoDb="org.Mm.eg.db", level = "gene", overlap = "all")


H3K27ac_diff_sites_anno %>% as.data.frame %>% arrange(FDR, desc(Fold)) %>% head()
```

### intersect ChIP-seq and RNAseq data

```{r}
down_genes<- res_df %>% filter(log2FoldChange < 0, padj < 0.05) %>% .$symbol

down_H3K27ac_genes<- as.data.frame(H3K27ac_diff_sites_anno) %>% filter(Fold <0, FDR < 0.05) %>% .$SYMBOL

intersect(down_genes, down_H3K27ac_genes)


H3K27ac_rnaseq_merge<- left_join(as.data.frame(H3K27ac_diff_sites_anno), res_df, by = c("SYMBOL" = "symbol"))
H3K4me1_rnaseq_merge<- left_join(as.data.frame(H3K4me1_diff_sites_anno), res_df, by = c("SYMBOL" = "symbol"))

write_tsv(H3K27ac_rnaseq_merge, "~/projects/Hunai_RNAseq/results/STAT-htseq/H3K27ac_htseq_rnaseq_merge.tsv")
write_tsv(H3K4me1_rnaseq_merge, "~/projects/Hunai_RNAseq/results/STAT-htseq/H3K4me1_htseq_rnaseq_merge.tsv")


H3K27ac_rnaseq_merge %>% filter(Fold <0, log2FoldChange <0, padj < 0.1) %>% arrange(desc(abs(Fold))) %>% head(n=100)

H3K27ac_rnaseq_merge %>% filter(SYMBOL == "Per2")

H3K4me1_rnaseq_merge %>% filter(Fold <0, log2FoldChange <0, padj < 0.1) %>% arrange(desc(abs(Fold))) %>% head(n=100)

H3K27ac_rnaseq_merge %>% filter(abs(Fold)>1, abs(log2FoldChange) >1, FDR < 0.5, padj < 0.5)


H3K27ac_rnaseq_merge %>% 
  filter(abs(Fold)>1, abs(log2FoldChange) >1) %>% 
  ggplot(aes(x = log2FoldChange, y = Fold)) + 
  geom_point() +
  geom_text_repel(data= dplyr::filter(H3K27ac_rnaseq_merge, p.value < 0.1, pvalue < 0.1), aes(label=SYMBOL)) +
                theme_bw(base_size = 14)

H3K27ac_rnaseq_merge %>% 
  filter(Fold < -0, log2FoldChange < -0) %>% 
  select(seqnames, start, end) %>% write.table(file = "~/projects/Hunai_RNAseq/results/STAT-htseq/H3K27ac_down_htseq_down_regions.bed", sep  ="\t", row.names = F, col.names =F, quote =F)

H3K27ac_rnaseq_merge %>% 
  select(seqnames, start, end) %>% write.table(file = "~/projects/Hunai_RNAseq/results/STAT-htseq/H3K27ac_all_regions.bed", sep  ="\t", row.names = F, col.names =F, quote =F)

H3K27ac_rnaseq_merge %>% filter(Fold > 1 &log2FoldChange > 0.9 | Fold < -0.9 & log2FoldChange < -1) %>% View()

H3K27ac_rnaseq_merge %>% filter(SYMBOL == "Shank2")

H3K27ac_rnaseq_merge %>% filter(SYMBOL == "Per2")

H3K27ac_rnaseq_merge %>% filter(SYMBOL == "CDK4")
  
H3K27ac_rnaseq_merge %>% 
  filter(abs(Fold)>1, abs(log2FoldChange) >1) %>% 
  ggplot(aes(x = log2FoldChange, y = Fold)) + geom_point() + theme_bw(base_size = 14)

H3K27ac_rnaseq_merge %>% 
  filter(Fold <0, log2FoldChange <0) %>%
  select(SYMBOL, Fold, log2FoldChange) %>% 
  filter(SYMBOL %in% c("Arid3a", "Casz1", "Epas1", "Mkl2", 
                       "Nfic", "Per2", "Smad6", "Sox6", "Tal1", "Tbx4"))

H3K4me1_rnaseq_merge %>% 
  filter(padj < 0.1, FDR < 0.1) %>% 
  ggplot(aes(x = log2FoldChange, y = Fold)) + 
  geom_point() +
  geom_text_repel(data= dplyr::filter(H3K4me1_rnaseq_merge, padj < 0.01, FDR < 0.01), aes(label=SYMBOL)) +
                theme_bw(base_size = 14)


H3K4me1_rnaseq_merge %>% 
  filter(abs(Fold) > 1, abs(log2FoldChange) >1) %>% 
  ggplot(aes(x = log2FoldChange, y = Fold)) + 
  geom_point() + theme_bw(base_size = 14)


H3K27ac_down_peaks<- H3K27ac_rnaseq_merge %>% filter(Fold <0, log2FoldChange <0, padj < 0.1 ) %>% select(seqnames, start, end) %>% makeGRangesFromDataFrame()

H3K4me1_down_peaks<- H3K4me1_rnaseq_merge %>% filter(Fold <0, log2FoldChange <0, padj < 0.1 ) %>% select(seqnames, start, end) %>% makeGRangesFromDataFrame()

overlap.hits<- findOverlaps(H3K27ac_down_peaks, H3K4me1_down_peaks)
both_H3K27ac_H3K4me1_down<- H3K27ac_down_peaks[queryHits(overlap.hits)] %>% as.data.frame() %>% left_join(H3K27ac_rnaseq_merge)

write_tsv(both_H3K27ac_H3K4me1_down, "~/projects/Hunai_RNAseq/results/both_H3K27ac_H3K4me1_down.tsv")
```


### super-enhancer

```{r}
library(DiffBind)
load("~/projects/Hunai_RNAseq/results/diffbind/UCI_H3K27ac_super_diffbind.rda")
load("~/projects/Hunai_RNAseq/results/diffbind/UCI_H3K4me1_super_diffbind.rda")
plot(UCI_H3K27ac_super)
plot(UCI_H3K4me1_super)

dba.plotPCA(UCI_H3K27ac_super,DBA_CONDITION,label=DBA_CONDITION)

dba.plotPCA(UCI_H3K4me1_super,DBA_CONDITION,label=DBA_CONDITION)

dba.plotPCA(UCI_H3K27ac_super, contrast=1,label=DBA_CONDITION)

dba.plotPCA(UCI_H3K4me1_super, contrast=1,label=DBA_CONDITION)

### annotate with ChIPseeker
library(ChIPseeker)
UCI_H3K4me1_super.DB
UCI_H3K27ac_super.DB


library(TxDb.Mmusculus.UCSC.mm9.knownGene)
library(org.Mm.eg.db)
txdb <- TxDb.Mmusculus.UCSC.mm9.knownGene
H3K27ac_diff_sites_super_anno <- annotatePeak(UCI_H3K27ac_super.DB, tssRegion=c(-3000, 3000), 
                         TxDb=txdb, annoDb="org.Mm.eg.db", level = "gene", overlap = "all",
                         addFlankGeneInfo = TRUE,  flankDistance = 200000)

H3K4me1_diff_sites_super_anno <- annotatePeak(UCI_H3K4me1_super.DB, tssRegion=c(-3000, 3000), 
                         TxDb=txdb, annoDb="org.Mm.eg.db", level = "gene", overlap = "all",
                         addFlankGeneInfo = TRUE,  flankDistance = 200000)

View(H3K27ac_diff_sites_super_anno)

H3K27ac_diff_sites_super_anno<- H3K27ac_diff_sites_super_anno %>% 
  as.data.frame()%>% 
  dplyr::select(seqnames,start, end, Fold, p.value, FDR, SYMBOL, flank_geneIds, flank_gene_distances) %>%
  mutate(flank_geneIds =strsplit(as.character(flank_geneIds), ";")) %>% 
  mutate(flank_gene_distances =strsplit(as.character(flank_gene_distances), ";")) %>%
  unnest(flank_geneIds, flank_gene_distances) 

H3K4me1_diff_sites_super_anno<- H3K4me1_diff_sites_super_anno %>% 
  as.data.frame()%>% 
  dplyr::select(seqnames,start, end, Fold, p.value, FDR, SYMBOL, flank_geneIds, flank_gene_distances) %>%
  mutate(flank_geneIds =strsplit(as.character(flank_geneIds), ";")) %>% 
  mutate(flank_gene_distances =strsplit(as.character(flank_gene_distances), ";")) %>%
  unnest(flank_geneIds, flank_gene_distances) 



H3K27ac_super_flank_gene_symbols<- AnnotationDbi::select(org.Mm.eg.db, keys=H3K27ac_diff_sites_super_anno$flank_geneIds, 
                                    columns="SYMBOL", keytype="ENTREZID") %>%
  dplyr::rename(flank_symbol= SYMBOL)

H3K4me1_super_flank_gene_symbols<- AnnotationDbi::select(org.Mm.eg.db, keys=H3K4me1_diff_sites_super_anno$flank_geneIds, 
                                    columns="SYMBOL", keytype="ENTREZID") %>%
  dplyr::rename(flank_symbol= SYMBOL)



H3K27ac_diff_sites_super_anno$flank_gene_symbol<- H3K27ac_super_flank_gene_symbols$flank_symbol
H3K4me1_diff_sites_super_anno$flank_gene_symbol<- H3K4me1_super_flank_gene_symbols$flank_symbol

```

### intersect ChIP-seq and RNAseq data

```{r}


H3K27ac_super_rnaseq_merge<- left_join(as.data.frame(H3K27ac_diff_sites_super_anno), res_df, by = c("flank_gene_symbol" = "symbol"))

H3K4me1_super_rnaseq_merge<- left_join(as.data.frame(H3K4me1_diff_sites_super_anno), res_df, by = c("flank_gene_symbol" = "symbol"))

write_tsv(H3K27ac_super_rnaseq_merge, "~/projects/Hunai_RNAseq/results/STAT-htseq/DESeq2/H3K27ac_super_htseq_rnaseq_200kb_flank_merge.tsv")
write_tsv(H3K4me1_super_rnaseq_merge, "~/projects/Hunai_RNAseq/results/STAT-htseq/DESeq2/H3K4me1_super_htseq_rnaseq_200kb_flank_merge.tsv")


H3K27ac_super_rnaseq_merge %>% filter(Fold <0, log2FoldChange <0, padj <0.1) %>% arrange(desc(abs(Fold))) %>% head()

H3K27ac_super_rnaseq_merge %>% filter(Fold <0, log2FoldChange <0) %>% arrange(Fold)%>% View() 

H3K27ac_super_rnaseq_merge %>% filter(SYMBOL == "Per2")
H3K27ac_super_rnaseq_merge %>% filter(flank_gene_symbol == "Notch4")
  
  
H3K27ac_super_rnaseq_merge %>% 
  filter(abs(Fold) >1, abs(log2FoldChange) > 0.7 ) %>% 
  ggplot(aes(x = log2FoldChange, y = Fold)) + 
  geom_point() +
  geom_text_repel(data= dplyr::filter(H3K27ac_super_rnaseq_merge, abs(Fold) >1, abs(log2FoldChange) > 0.7), aes(label=flank_gene_symbol)) +
                theme_bw(base_size = 14) +
  xlab("log2 fold change for RNA expression") +
  ylab("log2 fold change for superEnhancer signal") +
  ggtitle("H3K27ac super-enhancer and RNAseq 200kb")

H3K4me1_super_rnaseq_merge %>% 
  filter(abs(Fold) >1, abs(log2FoldChange) > 0.7) %>% 
  ggplot(aes(x = log2FoldChange, y = Fold)) + 
  geom_point() +
  geom_text_repel(data= dplyr::filter(H3K4me1_super_rnaseq_merge, abs(Fold) >1, abs(log2FoldChange) > 0.7), aes(label= flank_gene_symbol)) +
                theme_bw(base_size = 14) +
  xlab("log2 fold change for RNA expression") +
  ylab("log2 fold change for superEnhancer signal") +
  ggtitle("H3K4me1 super-enhancer and RNAseq 200 kb")


H3K27ac_super_down_peaks<- H3K27ac_super_rnaseq_merge %>% filter(Fold <0, log2FoldChange <0, padj < 0.1 ) %>% select(seqnames, start, end) %>% makeGRangesFromDataFrame()

H3K4me1_super_down_peaks<- H3K4me1_super_rnaseq_merge %>% filter(Fold <0, log2FoldChange <0, padj < 0.1 ) %>% select(seqnames, start, end) %>% makeGRangesFromDataFrame()

overlap_super.hits<- findOverlaps(H3K27ac_super_down_peaks, H3K4me1_super_down_peaks)
both_H3K27ac_H3K4me1_super_down<- H3K27ac_super_down_peaks[queryHits(overlap_super.hits)] %>% as.data.frame() %>% left_join(H3K27ac_super_rnaseq_merge)

write_tsv(both_H3K27ac_H3K4me1_super_down, "~/projects/Hunai_RNAseq/results/both_H3K27ac_H3K4me1_super_down.tsv")
```

### Heatmap

```{r}

H3K27ac_rnaseq_merge %>% filter(Fold > 1 &log2FoldChange > 0.7 | Fold < -1 & log2FoldChange < -0.7) %>% View()
H3K4me1_rnaseq_merge %>% filter(Fold > 1 &log2FoldChange > 0.7 | Fold < -1 & log2FoldChange < -0.7) %>% View()

H3K27ac_super_rnaseq_merge %>% filter(Fold > 1 &log2FoldChange > 0.7 | Fold < -1 & log2FoldChange < -0.7) %>% arrange(Fold)%>% View() 

H3K4me1_super_rnaseq_merge %>% filter(Fold > 1 &log2FoldChange > 0.7 | Fold < -1 & log2FoldChange < -0.7) %>% arrange(Fold)%>% View() 

head(assay(rld)[rownames(assay(rld)) != "", ])

H3K27ac_rnaseq_merge %>% head()

H3K27ac_bed<- H3K27ac_rnaseq_merge %>% 
  filter(Fold > 1 &log2FoldChange > 0.7 | Fold < -1 & log2FoldChange < -0.7) %>% 
  select(seqnames, start, end) %>% makeGRangesFromDataFrame()
dir("~/projects/Hunai_RNAseq/results/ChIPseq_bigwig/") 

library(rtracklayer)
MLL_1576_H3K27ac_bw<- import("~/projects/Hunai_RNAseq/results/ChIPseq_bigwig/MLL_1576-H3K27Ac.sorted.bw", format = "bigWig")

MLL_1527_H3K27ac_bw<- import("~/projects/Hunai_RNAseq/results/ChIPseq_bigwig/RAS_MLL-1527-H3K27Ac.sorted.bw", format = "bigWig")

RAS_1475_H3K27ac_bw<- import("~/projects/Hunai_RNAseq/results/ChIPseq_bigwig/RAS_1475-H3K27Ac.sorted.bw", format = "bigWig")

RAS_1508_H3K27ac_bw<- import("~/projects/Hunai_RNAseq/results/ChIPseq_bigwig/RAS_1508-1-H3K27Ac.sorted.bw", format = "bigWig")

H3K27ac.center<- resize(H3K27ac_bed, width =1, fix = "center")


library(EnrichedHeatmap)
library(ComplexHeatmap)

RAS_1475.mat<- normalizeToMatrix(RAS_1475_H3K27ac_bw, H3K27ac.center, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
RAS_1508.mat<- normalizeToMatrix(RAS_1508_H3K27ac_bw, H3K27ac.center, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
MLL_1576.mat<-normalizeToMatrix(MLL_1576_H3K27ac_bw, H3K27ac.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

MLL_1527.mat<-normalizeToMatrix(MLL_1527_H3K27ac_bw, H3K27ac.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

## plot H3K4me1 side by side
RAS_1475.K4me1.mat<- normalizeToMatrix(RAS_1475_H3K4me1_bw, H3K27ac.center, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
RAS_1508.K4me1.mat<- normalizeToMatrix(RAS_1508_H3K4me1_bw, H3K27ac.center, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
MLL_1576.K4me1.mat<-normalizeToMatrix(MLL_1576_H3K4me1_bw, H3K27ac.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

MLL_1527.K4me1.mat<-normalizeToMatrix(MLL_1527_H3K4me1_bw, H3K27ac.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

set.seed(123)

library(circlize)
quantile(RAS_1475.mat, probs = c(0.005, 0.5,0.995))
quantile(RAS_1508.mat, probs = c(0.005, 0.5,0.995))
quantile(MLL_1576.mat, probs = c(0.005, 0.5,0.995))
quantile(MLL_1527.mat, probs = c(0.005, 0.5,0.995))


quantile(RAS_1475.K4me1.mat, probs = c(0.005, 0.5,0.995))
quantile(RAS_1508.K4me1.mat, probs = c(0.005, 0.5,0.995))
quantile(MLL_1576.K4me1.mat, probs = c(0.005, 0.5,0.995))
quantile(MLL_1527.K4me1.mat, probs = c(0.005, 0.5,0.995))


col_fun2<- circlize::colorRamp2(c(0, 40), c("white", "red"))
col_fun3<- circlize::colorRamp2(c(0, 30), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 14),
             heatmap_legend_title_gp = gpar(fontsize = 14),
             heatmap_legend_labels_gp = gpar(fontsize = 14))


EnrichedHeatmap(RAS_1475.mat, axis_name_rot = 0, name = "RAS_1475 H3K27ac",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun2, pos_line =F, axis_name_gp = gpar(fontsize = 14), 
                axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(RAS_1508.mat, axis_name_rot = 0, name = "RAS_1508 H3K27ac", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), 
                        axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1576.mat, axis_name_rot = 0, name = "MLL_1576 H3K27ac", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1527.mat, axis_name_rot = 0, name = "MLL_1527 H3K27ac", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun2, pos_line =F,
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb")) 

genes<- H3K27ac_rnaseq_merge %>% filter(Fold > 1 &log2FoldChange > 0.7 | Fold < -1 & log2FoldChange < -0.7) %>% .$SYMBOL

rna_mat<- assay(rld)[genes, c(6,5,4,3)] 

hp1<- Heatmap(t(scale(t(rna_mat), center =T, scale =F)), cluster_rows = T, clustering_distance_rows = "pearson",
        cluster_columns = F, show_row_names = F, name = "RNA experssion", width = 4, use_raster = TRUE) +
  EnrichedHeatmap(RAS_1475.mat, axis_name_rot = 45, name = "RAS_1475 H3K27ac",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun2, pos_line =F, axis_name_gp = gpar(fontsize = 14), 
                axis_name = c("-5kb", "center", "5kb"), width = 1) +
        EnrichedHeatmap(RAS_1508.mat, axis_name_rot = 45, name = "RAS_1508 H3K27ac", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), 
                        axis_name = c("-5kb", "center", "5kb"), width = 1) +
        EnrichedHeatmap(MLL_1576.mat, axis_name_rot = 45, name = "MLL_1576 H3K27ac", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb"), width = 1) +
        EnrichedHeatmap(MLL_1527.mat, axis_name_rot = 45, name = "MLL_1527 H3K27ac", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun2, pos_line =F,
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb"), width = 1) + 
  EnrichedHeatmap(RAS_1475.K4me1.mat, axis_name_rot = 45, name = "RAS_1475 H3K4me1",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun3, pos_line =F, axis_name_gp = gpar(fontsize = 14), 
                axis_name = c("-5kb", "center", "5kb"), width = 1) +
        EnrichedHeatmap(RAS_1508.K4me1.mat, axis_name_rot = 45, name = "RAS_1508 H3K4me1", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun3, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), 
                        axis_name = c("-5kb", "center", "5kb"), width = 1) +
        EnrichedHeatmap(MLL_1576.K4me1.mat, axis_name_rot = 45, name = "MLL_1576 H3K4me1", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun3, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb"), width = 1) +
        EnrichedHeatmap(MLL_1527.K4me1.mat, axis_name_rot = 45, name = "MLL_1527 H3K4me1", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun3, pos_line =F,
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb"), width = 1)
    

rna_mat[row_order(hp1)[[1]],]

hp1
```


For H3K4me1

```{r}
H3K4me1_bed<- H3K4me1_rnaseq_merge %>% 
  filter(Fold > 1 &log2FoldChange > 0.7 | Fold < -1 & log2FoldChange < -0.7) %>% 
  select(seqnames, start, end) %>% makeGRangesFromDataFrame()
dir("~/projects/Hunai_RNAseq/results/ChIPseq_bigwig/") 

library(rtracklayer)
MLL_1576_H3K4me1_bw<- import("~/projects/Hunai_RNAseq/results/ChIPseq_bigwig/MLL_1576-H3K4me1.sorted.bw", format = "bigWig")

MLL_1527_H3K4me1_bw<- import("~/projects/Hunai_RNAseq/results/ChIPseq_bigwig/RAS_MLL-1527-H3K4me1.sorted.bw", format = "bigWig")

RAS_1475_H3K4me1_bw<- import("~/projects/Hunai_RNAseq/results/ChIPseq_bigwig/RAS_1475-H3K4me1.sorted.bw", format = "bigWig")

RAS_1508_H3K4me1_bw<- import("~/projects/Hunai_RNAseq/results/ChIPseq_bigwig/RAS_1508-1-H3K4me1.sorted.bw", format = "bigWig")

H3K4me1.center<- resize(H3K4me1_bed, width =1, fix = "center")

library(EnrichedHeatmap)
library(ComplexHeatmap)

RAS_1475.mat2<- normalizeToMatrix(RAS_1475_H3K4me1_bw, H3K4me1.center, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
RAS_1508.mat2<- normalizeToMatrix(RAS_1508_H3K4me1_bw, H3K4me1.center, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
MLL_1576.mat2<-normalizeToMatrix(MLL_1576_H3K4me1_bw, H3K4me1.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

MLL_1527.mat2<-normalizeToMatrix(MLL_1527_H3K4me1_bw, H3K4me1.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)


RAS_1475.H3K27ac.mat2<- normalizeToMatrix(RAS_1475_H3K27ac_bw, H3K4me1.center, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
RAS_1508.H3K27ac.mat2<- normalizeToMatrix(RAS_1508_H3K27ac_bw, H3K4me1.center, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
MLL_1576.H3K27ac.mat2<-normalizeToMatrix(MLL_1576_H3K27ac_bw, H3K4me1.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

MLL_1527.H3K27ac.mat2<-normalizeToMatrix(MLL_1527_H3K27ac_bw, H3K4me1.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

set.seed(123)

library(circlize)
quantile(RAS_1475.mat2, probs = c(0.005, 0.5,0.995))
quantile(RAS_1508.mat2, probs = c(0.005, 0.5,0.995))
quantile(MLL_1576.mat2, probs = c(0.005, 0.5,0.995))
quantile(MLL_1527.mat2, probs = c(0.005, 0.5,0.995))

col_fun1<- circlize::colorRamp2(c(0, 30), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 14),
             heatmap_legend_title_gp = gpar(fontsize = 14),
             heatmap_legend_labels_gp = gpar(fontsize = 14))




EnrichedHeatmap(RAS_1475.mat2, axis_name_rot = 0, name = "RAS_1475 H3K4me1",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun2, pos_line =F, axis_name_gp = gpar(fontsize = 14), 
                axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(RAS_1508.mat2, axis_name_rot = 0, name = "RAS_1508 H3K4me1", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), 
                        axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1576.mat2, axis_name_rot = 0, name = "MLL_1576 H3K4me1", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1527.mat2, axis_name_rot = 0, name = "MLL_1527 H3K4me1", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun2, pos_line =F,
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb")) 

genes2<- H3K4me1_rnaseq_merge %>% filter(Fold > 1 &log2FoldChange > 0.7 | Fold < -1 & log2FoldChange < -0.7) %>% .$SYMBOL

rna_mat2<- assay(rld)[genes2, c(6,5,4,3)] 

hp2<- Heatmap(t(scale(t(rna_mat2), center =T, scale =F)), cluster_rows = T, clustering_distance_rows = "pearson",
        cluster_columns = F, show_row_names = F, name = "RNA experssion", width = 4, use_raster = TRUE) +
  EnrichedHeatmap(RAS_1475.mat2, axis_name_rot = 45, name = "RAS_1475 H3K4me1",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun1, pos_line =F, axis_name_gp = gpar(fontsize = 14), 
                axis_name = c("-5kb", "center", "5kb"), width = 1) +
        EnrichedHeatmap(RAS_1508.mat2, axis_name_rot = 45, name = "RAS_1508 H3K4me1", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun1, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), 
                        axis_name = c("-5kb", "center", "5kb"), width = 1) +
        EnrichedHeatmap(MLL_1576.mat2, axis_name_rot = 45, name = "MLL_1576 H3K4me1", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun1, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb"), width = 1) +
        EnrichedHeatmap(MLL_1527.mat2, axis_name_rot = 45, name = "MLL_1527 H3K4me1", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun1, pos_line =F,
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb"), width = 1) +
  EnrichedHeatmap(RAS_1475.H3K27ac.mat2, axis_name_rot = 45, name = "RAS_1475 H3K27ac",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun2, pos_line =F, axis_name_gp = gpar(fontsize = 14), 
                axis_name = c("-5kb", "center", "5kb"), width = 1) +
        EnrichedHeatmap(RAS_1508.H3K27ac.mat2, axis_name_rot = 45, name = "RAS_1508 H3K27ac", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), 
                        axis_name = c("-5kb", "center", "5kb"), width = 1) +
        EnrichedHeatmap(MLL_1576.H3K27ac.mat2, axis_name_rot = 45, name = "MLL_1576 H3K27ac", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb"), width = 1) +
        EnrichedHeatmap(MLL_1527.H3K27ac.mat2, axis_name_rot = 45, name = "MLL_1527 H3K27ac", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun2, pos_line =F,
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb"), width = 1)

hp2
```


### superEnhancer heatmap

```{r}

H3K27ac_super_rnaseq_merge %>% filter(Fold > 1 &log2FoldChange > 0.7 | Fold < -1 & log2FoldChange < -0.7) %>% arrange(Fold)%>% View() 

H3K4me1_super_rnaseq_merge %>% filter(Fold > 1 &log2FoldChange > 0.7 | Fold < -1 & log2FoldChange < -0.7) %>% arrange(Fold)%>% View() 

head(assay(rld)[rownames(assay(rld)) != "", ])

H3K27ac_rnaseq_merge %>% head()

H3K27ac_super_bed<- H3K27ac_super_rnaseq_merge %>% 
  filter(Fold > 1 &log2FoldChange > 0.7 | Fold < -1 & log2FoldChange < -0.7) %>% 
  select(seqnames, start, end) %>% makeGRangesFromDataFrame()
dir("~/projects/Hunai_RNAseq/results/ChIPseq_bigwig/") 

library(rtracklayer)
MLL_1576_H3K27ac_bw<- import("~/projects/Hunai_RNAseq/results/ChIPseq_bigwig/MLL_1576-H3K27Ac.sorted.bw", format = "bigWig")

MLL_1527_H3K27ac_bw<- import("~/projects/Hunai_RNAseq/results/ChIPseq_bigwig/RAS_MLL-1527-H3K27Ac.sorted.bw", format = "bigWig")

RAS_1475_H3K27ac_bw<- import("~/projects/Hunai_RNAseq/results/ChIPseq_bigwig/RAS_1475-H3K27Ac.sorted.bw", format = "bigWig")

RAS_1508_H3K27ac_bw<- import("~/projects/Hunai_RNAseq/results/ChIPseq_bigwig/RAS_1508-1-H3K27Ac.sorted.bw", format = "bigWig")


library(EnrichedHeatmap)
library(ComplexHeatmap)

RAS_1475.mat3<- normalizeToMatrix(RAS_1475_H3K27ac_bw, H3K27ac_super_bed, value_column = "score",
                                  mean_mode="w0", w=100, extend = 2000, target_ratio = 0.6)
RAS_1508.mat3<- normalizeToMatrix(RAS_1508_H3K27ac_bw, H3K27ac_super_bed, value_column = "score",
                                  mean_mode="w0", w=100, extend = 2000, target_ratio = 0.6)
MLL_1576.mat3<-normalizeToMatrix(MLL_1576_H3K27ac_bw, H3K27ac_super_bed, value_column = "score",
                                 mean_mode="w0", w=100, extend = 2000, target_ratio = 0.6)

MLL_1527.mat3<-normalizeToMatrix(MLL_1527_H3K27ac_bw, H3K27ac_super_bed, value_column = "score",
                                 mean_mode="w0", w=100, extend = 2000, target_ratio = 0.6)

set.seed(123)

library(circlize)
quantile(RAS_1475.mat3, probs = c(0.005, 0.5,0.995))
quantile(RAS_1508.mat3, probs = c(0.005, 0.5,0.995))
quantile(MLL_1576.mat3, probs = c(0.005, 0.5,0.995))
quantile(MLL_1527.mat3, probs = c(0.005, 0.5,0.995))

col_fun2<- circlize::colorRamp2(c(0, 60), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 14),
             heatmap_legend_title_gp = gpar(fontsize = 14),
             heatmap_legend_labels_gp = gpar(fontsize = 14))




EnrichedHeatmap(RAS_1475.mat3, axis_name_rot = 0, name = "RAS_1475 H3K27ac",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun2, pos_line =F, axis_name_gp = gpar(fontsize = 14), 
                axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(RAS_1508.mat3, axis_name_rot = 0, name = "RAS_1508 H3K27ac", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), 
                        axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1576.mat3, axis_name_rot = 0, name = "MLL_1576 H3K27ac", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1527.mat3, axis_name_rot = 0, name = "MLL_1527 H3K27ac", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun2, pos_line =F,
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb")) 

genes3<- H3K27ac_super_rnaseq_merge %>% filter(Fold > 1 &log2FoldChange > 0.7 | Fold < -1 & log2FoldChange < -0.7) %>% .$SYMBOL

rna_mat3<- assay(rld)[genes3, c(6,5,4,3)] 

hp3<- Heatmap(t(scale(t(rna_mat3), center =T, scale =F)), cluster_rows = T, clustering_distance_rows = "pearson",
        cluster_columns = F, show_row_names = F, name = "RNA experssion", width = 4, use_raster = TRUE) +
  EnrichedHeatmap(RAS_1475.mat3, axis_name_rot = 45, name = "RAS_1475 H3K27ac",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun2, pos_line =F, axis_name_gp = gpar(fontsize = 14), 
                axis_name = c("-2kb", "start", "end", "2kb"), width = 1) +
        EnrichedHeatmap(RAS_1508.mat3, axis_name_rot = 45, name = "RAS_1508 H3K27ac", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), 
                        axis_name = c("-2kb", "start", "end", "2kb"), width = 1) +
        EnrichedHeatmap(MLL_1576.mat3, axis_name_rot = 45, name = "MLL_1576 H3K27ac", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-2kb", "start", "end", "2kb"), width = 1) +
        EnrichedHeatmap(MLL_1527.mat3, axis_name_rot = 45, name = "MLL_1527 H3K27ac", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun2, pos_line =F,
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-2kb", "start", "end", "2kb"), width = 1)

hp3
rna_mat[row_order(hp1)[[1]],]
```


### Enhancer RNA

```{r}
library(rtracklayer)
library(EnrichedHeatmap)
base<- "~/projects/Hunai_RNAseq/results/RNAseq_bigwig"
MLL_1612_RNA_bw<- import(file.path(base, "3-Mll4-RasG12D-1612-2-cd45_S39.bw"), format = "bigwig")
MLL_1646_RNA_bw<- import(file.path(base, "4-Mll4-RasG12D-1646-2-cd45_S40.bw"), format = "bigwig")

RAS_1665_RNA_bw<- import(file.path(base, "5-RasG12D-1665-cd45_S41.bw"), format = "bigwig")
RAS_1507_RNA_bw<- import(file.path(base, "6--RasG12D-1507-cd45_S42.bw"), format = "bigwig")

transcribed_enhancers<- import("/mnt/nautilus/hunai-cd45/hunai_downsample/SKCM_snakemake_ChIPseq/12chromHMM/MYOUTPUT/transcribed_enhancers.bed", format = "Bed")

transcribed_enhancers.center<- resize(transcribed_enhancers, width =1, fix = "center")
RAS_1665.mat<- normalizeToMatrix(RAS_1665_RNA_bw, transcribed_enhancers.center, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
RAS_1646.mat<- normalizeToMatrix(RAS_1507_RNA_bw, transcribed_enhancers.center, value_column = "score",
                                  mean_mode="w0", w=100, extend = 5000)
MLL_1612.mat<-normalizeToMatrix(MLL_1612_RNA_bw, transcribed_enhancers.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

MLL_1646.mat<-normalizeToMatrix(MLL_1646_RNA_bw, transcribed_enhancers.center, value_column = "score",
                                 mean_mode="w0", w=100, extend = 5000)

set.seed(123)

library(circlize)
quantile(RAS_1665.mat, probs = c(0.005, 0.5,0.995))
quantile(RAS_1646.mat, probs = c(0.005, 0.5,0.995))
quantile(MLL_1612.mat, probs = c(0.005, 0.5,0.995))
quantile(MLL_1646.mat, probs = c(0.005, 0.5,0.995))

col_fun2<- circlize::colorRamp2(c(0, 40), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 14),
             heatmap_legend_title_gp = gpar(fontsize = 14),
             heatmap_legend_labels_gp = gpar(fontsize = 14))



EnrichedHeatmap(RAS_1665.mat, axis_name_rot = 0, name = "RAS_1665 eRNA",
                column_title = "RAS_1665", use_raster = TRUE, 
                col= col_fun2, pos_line =F, axis_name_gp = gpar(fontsize = 14), 
                axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(RAS_1646.mat, axis_name_rot = 0, name = "RAS_1646 eRNA", 
                        column_title ="RAS_1646", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14), 
                        axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1612.mat, axis_name_rot = 0, name = "MLL_1612 eRNA", 
                        column_title = "MLL_1612", use_raster = TRUE, col= col_fun2, pos_line =F, 
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(MLL_1646.mat, axis_name_rot = 0, name = "MLL_1646 eRNA", 
                        column_title = "MLL_1646", use_raster = TRUE, col= col_fun2, pos_line =F,
                        axis_name_gp = gpar(fontsize = 14),
                        axis_name = c("-5kb", "center", "5kb"))

```
