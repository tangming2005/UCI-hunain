### analysis notes for the mouse MLL4 and KDM2A knockout ChIP-seq data

>In brief, we are investigating the role of MLL4 and KDM2A (using floxed-MLL4 or -KDM2A mice ) in lung cancer using the LSL-K-Ras mouse model. 
Mll4 is a H3K4 methyltransferase (a transcriptional co-activator), acts as a tumor suppressors genes in lung cancer and our data suggest that deletion of Mll4 
increases the aggressiveness of RAS-driven tumors. On the other hand, KDM2A is a H3H36 demethylase (a transcriptional co-repressor), acts as an oncogene in 
lung cancer and its deletion inhibits lung tumorigenesis. We have performed RNA-seq to compare gene expression profile of a tumor derived from RASMLL4  
(MLL4-deleted tumor)  and RASKDM2A(KDM2A-deleted tumor)   with controls tumors (RNA-Seq data are available with Kunal). 
We would like to do a comprehensive analysis of epigenetic changes (by analyzing these important histone marks at different regions) at genome-wide 
levels to understand the molecular mechanism associated with its (MLL4) tumor suppressive and KDM2A oncogenic functions. We have discussed our experimental 
design and epigenetic analysis part with Kunal in detail. In fact, he has suggested some of the marks to include in the ChIP-Seq analysis.

### Sample information

| FastQ_file_name                     | Sample       | Antibody | 
|-------------------------------------|--------------|----------| 
| R291-L2-P1-CAGATC-Sequences.txt.gz  | KD_1031      | MLL2     | 
| R291-L2-P2-GATCAG-Sequences.txt.gz  | RAS_1508  | H3K4me1  | 
| R291-L2-P3-TAGCTT-Sequences.txt.gz  | RAS_1508   | H3K9me3  | 
| R291-L2-P4-GGCTAC-Sequences.txt.gz  | RAS_1508   | H3K27Ac  | 
| R291-L2-P5-CTTGTA-Sequences.txt.gz  | RAS_1508   | H3K79me2 | 
| R291-L2-P6-AGTCAA-Sequences.txt.gz  | RAS_1508   | H3K4me3  | 
| R291-L2-P7-AGTTCC-Sequences.txt.gz  | RAS_1508   | H3K27me3 | 
| R291-L2-P8-ACTTGA-Sequences.txt.gz  | RAS_1508   | MLL2     | 
| R291-L2-PrNotRecog-Sequences.txt.gz |              |          | 
| R291-L3-P1-ATGTCA-Sequences.txt.gz  | RAS_1508   | H3K4me2  | 
| R291-L3-P2-CCGTCC-Sequences.txt.gz  | RAS_1508   | Input    | 
| R291-L3-P3-GTCCGC-Sequences.txt.gz  | RAS_1475     | H3K4me1  | 
| R291-L3-P4-GTGAAA-Sequences.txt.gz  | RAS_1475     | H3K9me3  | 
| R291-L3-P5-GTGGCC-Sequences.txt.gz  | RAS_1475     | H3K27Ac  | 
| R291-L3-P6-GTTTCG-Sequences.txt.gz  | RAS_1475     | H3K79me2 | 
| R291-L3-P7-CGTACG-Sequences.txt.gz  | RAS_1475     | H3K4me3  | 
| R291-L3-P8-GATCAG-Sequences.txt.gz  | RAS_1475     | MLL2     | 
| R291-L3-PrNotRecog-Sequences.txt.gz |              |          | 
| R291-L4-P1-GAGTGG-Sequences.txt.gz  | RAS_1475     | H3K27me3 | 
| R291-L4-P2-ACTGAT-Sequences.txt.gz  | RAS_1475     | H3K4me2  | 
| R291-L4-P3-ATTCCT-Sequences.txt.gz  | RAS_1475     | Input    | 
| R291-L4-P4-ATCACG-Sequences.txt.gz  | MLL_1527 | H3K4me1  | 
| R291-L4-P5-CGATGT-Sequences.txt.gz  | MLL_1527 | H3K9me3  | 
| R291-L4-P6-TTAGGC-Sequences.txt.gz  | MLL_1527 | H3K27Ac  | 
| R291-L4-P7-TGACCA-Sequences.txt.gz  | MLL_1527 | H3K79me2 | 
| R291-L4-P8-TAGCTT-Sequences.txt.gz  | MLL_1527 | MLL2     | 
| R291-L4-PrNotRecog-Sequences.txt.gz |              |          | 
| R291-L5-P1-ACAGTG-Sequences.txt.gz  | MLL_1527 | H3K4me3  | 
| R291-L5-P2-GCCAAT-Sequences.txt.gz  | MLL_1527 | H3K27me3 | 
| R291-L5-P3-CAGATC-Sequences.txt.gz  | MLL_1527 | H3K4me2  | 
| R291-L5-P4-ACTTGA-Sequences.txt.gz  | MLL_1527 | Input    | 
| R291-L5-P5-GATCAG-Sequences.txt.gz  | KD_1011      | H3K4me1  | 
| R291-L5-P6-TAGCTT-Sequences.txt.gz  | KD_1011      | H3K9me3  | 
| R291-L5-P7-GGCTAC-Sequences.txt.gz  | KD_1011      | H3K27Ac  | 
| R291-L5-P8-CTTGTA-Sequences.txt.gz  | KD_1011      | H3K79me2 | 
| R291-L5-PrNotRecog-Sequences.txt.gz |              |          | 
| R291-L6-P1-AGTCAA-Sequences.txt.gz  | KD_1011      | H3K4me3  | 
| R291-L6-P2-AGTTCC-Sequences.txt.gz  | KD_1011      | H3K27me3 | 
| R291-L6-P3-ATGTCA-Sequences.txt.gz  | KD_1011      | H3K4me2  | 
| R291-L6-P4-CCGTCC-Sequences.txt.gz  | KD_1011      | Input    | 
| R291-L6-P5-GGCTAC-Sequences.txt.gz  | KD_1011      | MLL2     | 
| R291-L6-P6-CTTGTA-Sequences.txt.gz  | MLL_1576     | MLL2     | 
| R291-L6-P7-GTCCGC-Sequences.txt.gz  | MLL_1576     | H3K4me1  | 
| R291-L6-PrNotRecog-Sequences.txt.gz |              |          | 
| R291-L7-P1-GTGAAA-Sequences.txt.gz  | MLL_1576     | H3K9me3  | 
| R291-L7-P2-GTGGCC-Sequences.txt.gz  | MLL_1576     | H3K27Ac  | 
| R291-L7-P3-GTTTCG-Sequences.txt.gz  | MLL_1576     | H3K79me2 | 
| R291-L7-P4-CGTACG-Sequences.txt.gz  | MLL_1576     | H3K4me3  | 
| R291-L7-P5-GAGTGG-Sequences.txt.gz  | MLL_1576     | H3K27me3 | 
| R291-L7-P6-ACTGAT-Sequences.txt.gz  | MLL_1576     | H3K4me2  | 
| R291-L7-P7-ATTCCT-Sequences.txt.gz  | MLL_1576     | Input    | 
| R291-L7-PrNotRecog-Sequences.txt.gz |              |          | 


A: H3K4me1  
B: H3K9me3  
C: H3K27Ac  
D: H3K79me2  
E: H3K4me3  
F: H3K27me3  
G: Input  
H: MLL2
I: H3K4me2


### RNAseq data

Samir has processed the data using star-HTseq pipeline and put the results  

`/rsrch1/genomic_med/krai/PendingSort/UCI_DATA/`