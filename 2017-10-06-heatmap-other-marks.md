```bash
cd /rsrch2/genomic_med/krai/hunain_histone_reseq/snakemake_ChIPseq_pipeline_downsample/08peak_macs1/merged_peaks


# merge peaks (for H3K27me3, use the non-model output)
cat ../*F_vs_*nomodel_peaks.bed | sort -k1,1 -k2,2n | bedtools merge -i - -d 3000 > merged_F_peaks.bed

#for H3K4me3
cat ../*E_vs_*macs1_peaks.bed | sort -k1,1 -k2,2n | bedtools merge -i - -d 1000 > merged_E_peaks.bed

#for H3K4me1
cat ../*A_vs_*macs1_peaks.bed | sort -k1,1 -k2,2n | bedtools merge -i - -d 1000 > merged_A_peaks.bed

# for H3K9me3

cat ../*B_vs_*macs1_peaks.bed | sort -k1,1 -k2,2n | bedtools merge -i - -d 3000 > merged_B_peaks.bed

#for H3K27ac
cat ../*C_vs_*macs1_peaks.bed | sort -k1,1 -k2,2n | bedtools merge -i - -d 1000 > merged_C_peaks.bed


# for H3K79me2, gene body plot.
```


---
title: "2017-10-06_heatmap_hunai"
author: "ming tang"
date: "10/6/2017"
output: html_document
---

### Heatmap

```{r}
library(EnrichedHeatmap)
library(rtracklayer)
library(GenomicFeatures)

getCompositeMatrix<- function(target, width, ...){
        signals<- as.list(...)
        signal.names<- lapply(signals, basename)
        # assume those are bigwig files, and extract sample names before .bw
        sample.names<- gsub(".bw", "", unlist(signal.names))
        target<- import(target, format = "BED")
        include.bed<- resize(target, width = width, fix = "center")
        bigwigs<- lapply(signals, import, format = "bigWig", which = target)
        mats<- lapply(bigwigs, normalizeToMatrix, 
                      target = resize(target, width = 1, fix = "center"),
                      value_column = "score",
                      mean_mode="w0", w=100, extend = 1/2*width )
        names(mats)<- sample.names
        names(bigwigs)<- sample.names
        return (list(mats = mats, bigwigs = bigwigs))
}

bigwigs<- list.files("/mnt/shark/hunain_histone_reseq/snakemake_ChIPseq_pipeline_downsample/07bigwig/", pattern= "*F.bw", full.names = T)

mats_bw<- getCompositeMatrix(target = "/mnt/shark/hunain_histone_reseq/snakemake_ChIPseq_pipeline_downsample/08peak_macs1/merged_peaks/merged_F_peaks.bed", width = 10000, bigwigs)

peaks<-import("/mnt/shark/hunain_histone_reseq/snakemake_ChIPseq_pipeline_downsample/08peak_macs1/merged_peaks/merged_F_peaks.bed", format = "BED")

mats<- mats_bw$mats
bw<- mats_bw$bigwigs


lapply(mats, quantile, probs = c(0.005, 0.5,0.95))

mat_merge<- do.call(cbind, mats)
set.seed(100)
km<-kmeans(mat_merge, centers =3)$cluster

col_fun<- circlize::colorRamp2(c(0,25), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 15),
              heatmap_legend_title_gp = gpar(fontsize = 15),
              heatmap_legend_labels_gp = gpar(fontsize = 15))


pdf("~/projects/Hunai_RNAseq/results/H3K27me3_heatmap2.pdf", width = 8, height = 8)

EnrichedHeatmap(mats$RAS_1475_F, axis_name_rot = 0, name = "RAS_1475", 
                column_title = "RAS_1475", use_raster = TRUE,
                col = col_fun,
                split = km, cluster_rows = TRUE,
                axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(mats$RAS_1508_F, axis_name_rot = 0, name = "RAS_1508", 
                        column_title ="RAS_1508", use_raster = TRUE,
                        col = col_fun,
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(mats$MLL_1527_F, axis_name_rot = 0, name = "MLL1527", 
                        column_title = "MLL1527", use_raster = TRUE,
                        col = col_fun,
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "center", "5kb")) +
        EnrichedHeatmap(mats$MLL_1576_F, axis_name_rot = 0, name = "MLL_1576", 
                        column_title = "MLL_1576", use_raster = TRUE,
                        col = col_fun,
                        axis_name_gp = gpar(fontsize = 14), axis_name = c("-5kb", "center", "5kb"))

dev.off()

```

### line graph


```{r}


bind_rows(lapply(mats, colMeans))
bind_cols(lapply(mats, colMeans))

all<- bind_cols(lapply(mats, colMeans)) %>% 
  mutate(pos = rownames(RAS_1475_mean)) %>%
        gather(sample, value, 1:4) 

all$pos<- factor(all$pos, levels= rownames(RAS_1475_mean))

pdf("~/projects/Hunai_RNAseq/results/H3K27me3_line1.pdf", width = 4, height = 4)
ggplot(all, aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample), size = 2) + 
        theme_classic(base_size = 16) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "d1", "d50"), labels =c ("-5kb", "center", "5kb")) +
        xlab(NULL) + 
        ggtitle("H3K27me3 signal")
dev.off()

average<- all %>% separate(sample, c("sample", "replicate"), sep= 3) %>% 
        group_by(pos, sample) %>% 
        summarise(value = mean(value))

pdf("~/projects/Hunai_RNAseq/results/H3K27me3_line2.pdf", width = 4, height = 4)
ggplot(average, aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample), size = 2) + 
        theme_classic(base_size = 16) +
        theme(axis.ticks.x = element_blank()) +
        scale_x_discrete(breaks = c("u1", "d1", "d50"), labels =c ("-5kb", "center", "5kb")) +
        xlab(NULL) + 
        ggtitle("H3K27me3 signal")
dev.off()


```


### box plot

```{r}

bw<- mats_bw$bigwigs

binding_score<- lapply(bw, coverage, weight = "score")

binnedSum <- function(numvar, bins, mcolname)
{
  stopifnot(is(bins, "GRanges"))
  stopifnot(is(numvar, "RleList"))
  stopifnot(identical(seqlevels(bins), names(numvar)))
  bins_per_chrom <- split(ranges(bins), seqnames(bins))
  sums_list <- lapply(names(numvar),
      function(seqname) {
          views <- Views(numvar[[seqname]],
                         bins_per_chrom[[seqname]])
          viewSums(views)
      })
  new_mcol <- unsplit(sums_list, as.factor(seqnames(bins)))
  mcols(bins)[[mcolname]] <- new_mcol
  bins
}

## this function is in the GenomicRanges pacakge!, but bins comes first.

binnedAverage <- function(numvar, bins, mcolname)
{
  stopifnot(is(bins, "GRanges"))
  stopifnot(is(numvar, "RleList"))
  stopifnot(identical(seqlevels(bins), names(numvar)))
  bins_per_chrom <- split(ranges(bins), seqnames(bins))
  sums_list <- lapply(names(numvar),
      function(seqname) {
          views <- Views(numvar[[seqname]],
                         bins_per_chrom[[seqname]])
          viewMeans(views)
      })
  new_mcol <- unsplit(sums_list, as.factor(seqnames(bins)))
  mcols(bins)[[mcolname]] <- new_mcol
  bins
}

names(binding_score$MLL_1527_F)
seqlevels(peaks)

binding_score$MLL_1527_F[seqlevels(peaks)]

### reorder or subset the binding_score to have the same seqlevels as the peaks.

binding_score<- lapply(binding_score, function(x) x[seqlevels(peaks)])

## calculate the mean value of RPKM for each peak
binding_score_at_peaks<-lapply(binding_score, binnedAverage, peaks, "score")



binding_matrix<- bind_cols(lapply(binding_score_at_peaks, function(x) x$score))

binding_matrix<- binding_matrix %>% 
  gather(sample, value, 1:4) %>% 
  mutate(ID = sample) %>%
  separate(sample, c("treatment", "replicate", "mark"))

pdf("~/projects/Hunai_RNAseq/results/H3K27me3_box.pdf", width = 4, height = 4)
ggplot(binding_matrix, aes(x =ID, y = log2(value))) + 
               geom_boxplot(aes(fill = treatment)) +
        theme_classic(base_size = 16) +
        ggtitle("H3K27me3 signal for peaks") +
        ylab("log2 signal")
dev.off()


t.test(as.data.frame(H3K4me1_enhancer_all)$RAS_1508, as.data.frame(H3K4me1_enhancer_all)$MLL_1576, paired = T)

wilcox.test(as.data.frame(H3K4me1_enhancer_all)$RAS_1508, as.data.frame(H3K4me1_enhancer_all)$MLL_1576, paired = T)

```


