
Merge H3K4me1 superEnhancer for WT

```bash
cd /Users/mtang1/playground/hunai/superEnhancers/H3K4me1

cat  Sample_{5,6}* | sort -k1,1 -k2,2n | bedtools merge -i - > WT_merged_H3K4me1_superEnhancer.bed

annotatePeaks.pl WT_merged_H3K4me1_superEnhancer.bed mm9 -noann > WT_merged_H3K4me1_superEnhancer_annotation.txt

```


```r
###### H3K4me1 super-enhancer
setwd("~/playground/hunai/")
library(rtracklayer)
library(ComplexHeatmap)
H3K4me1_bed<- import("~/playground/hunai/superEnhancers/H3K4me1/WT_merged_H3K4me1_superEnhancer.bed", format = "BED")
H3K4me1.100kb<- resize(H3K4me1_bed, width = 100000, fix = "center")

## only read in a subset of the bigwig files using which argument!
RAS_1475.H3K4me1<- import("bigwigs/RAS_1475-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.100kb)
RAS_1508.H3K4me1<- import("bigwigs/RAS_1508-1-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.100kb)
MLL_1576.H3K4me1<- import("bigwigs/MLL_1576-H3K4me1.sorted.bw", format= "bigWig", which = H3K4me1.100kb)
MLL_1527.H3K4me1<- import("bigwigs/RAS_MLL-1527-H3K4me1.sorted.bw", format = "bigWig", which = H3K4me1.100kb)

library(EnrichedHeatmap)
H3K4me1.center<- resize(H3K4me1.100kb, width =1, fix = "center")

RAS_1475.mat1<- normalizeToMatrix(RAS_1475.H3K4me1, H3K4me1.center, value_column = "score",
                                  mean_mode="w0", w=1000, extend = 50000)
RAS_1508.mat1<- normalizeToMatrix(RAS_1508.H3K4me1, H3K4me1.center, value_column = "score",
                                  mean_mode="w0", w=1000, extend = 50000)
MLL_1576.mat1<-normalizeToMatrix(MLL_1576.H3K4me1, H3K4me1.center, value_column = "score",
                                 mean_mode="w0", w=1000, extend = 50000)

MLL_1527.mat1<-normalizeToMatrix(MLL_1527.H3K4me1, H3K4me1.center, value_column = "score",
                                 mean_mode="w0", w=1000, extend = 50000)

set.seed(123)

### mapping colors

library(circlize)
quantile(RAS_1475.mat1, probs = c(0.005, 0.5,0.995))
quantile(RAS_1508.mat1, probs = c(0.005, 0.5,0.995))
quantile(MLL_1576.mat1, probs = c(0.005, 0.5,0.995))
quantile(MLL_1527.mat1, probs = c(0.005, 0.5,0.995))

col_fun1<- circlize::colorRamp2(c(0, 30), c("white", "red"))

ht_global_opt(heatmap_column_names_gp = gpar(fontsize = 14),
              heatmap_legend_title_gp = gpar(fontsize = 14),
              heatmap_legend_labels_gp = gpar(fontsize = 14))

EnrichedHeatmap(RAS_1475.mat1, axis_name_rot = 0, name = "RAS_1475 H3K4me1",
                column_title = "RAS_1475", use_raster = TRUE, 
                col= col_fun1, pos_line =F, axis_name_gp = gpar(fontsize = 14)) +
        EnrichedHeatmap(RAS_1508.mat1, axis_name_rot = 0, name = "RAS_1508 H3K4me1", 
                        column_title ="RAS_1508", use_raster = TRUE, col= col_fun1, pos_line =F, axis_name_gp = gpar(fontsize = 14)) +
        EnrichedHeatmap(MLL_1576.mat1, axis_name_rot = 0, name = "MLL_1576 H3K4me1", 
                        column_title = "MLL_1576", use_raster = TRUE, col= col_fun1, pos_line =F, axis_name_gp = gpar(fontsize = 14)) +
        EnrichedHeatmap(MLL_1527.mat1, axis_name_rot = 0, name = "MLL_1527 H3K4me1", 
                        column_title = "MLL_1527", use_raster = TRUE, col= col_fun1, pos_line =F, axis_name_gp = gpar(fontsize = 14)) 




```